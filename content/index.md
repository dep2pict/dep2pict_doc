+++
Tags = ["Development","golang"]
Description = ""
date = "2017-02-27T17:44:22+01:00"
title = "index"
menu = "main"
Categories = ["Development","GoLang"]

+++

# Dep2pict: Drawing dependency structures

**Dep2pict** is a tool for drawing dependency syntactic structures.
The input format is CoNLL and available output formats are `svg`, `pdf` and `png`.

A few examples of the tool output are given below.
See [Installation](../installation) and [Run](../run) for usage instructions.

## Example from Sequoia with non-projective dependencies
![Sequoia example](/fr.svg)

## Example from UD_English with empty node (in purple) and enhanced dependencies (in blue and below words)
![Sequoia example](/en.svg)

## Example from UD_Russian-SynTagRus
![Sequoia example](/ru.svg)
