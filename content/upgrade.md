+++
Categories = ["Development","GoLang"]
Tags = ["Development","golang"]
date = "2018-10-17T09:39:23+02:00"
title = "upgrade"
Description = ""
menu = "main"

+++

# Upgrading to a new version

## Make sure that your opam is in version 2
The last version of **Dep2pict** requires that the **opam** tool is in version **2.0.0** or higher.

You can check your versions with the command `opam --version`.
It it's not version 2, re-install **opam** in version 2 with instructions steps 2 and 3 on the [Installation page](../installation).

## update the Dep2pict software

```sh
opam update
opam upgrade
```
