+++
date = "2017-02-27T22:21:02+01:00"
title = "installation"
+++

# Installation of the Dep2pict program

**Dep2pict** is implemented with the **[Ocaml](http://ocaml.org)** language.
It is easy to install on Linux or Mac OS&nbsp;X
(installation on Windows should be possible, but this is untested).

If **Dep2pict** is already installed, please consult the [upgrade page](../upgrade).

:warning: If you run into trouble using instructions of this page, feel free to send a mail to the [maintainer](mailto:Bruno.Guillaum@inria.fr?subject=Dep2pict installation).

## 1) Install prerequisite

### On Ubuntu or Debian

```bash
apt update && apt install libcairo2-dev wget m4 unzip librsvg2-bin curl bubblewrap
```

### On Mac OS&nbsp;X

 * Install **[Homebrew](https://brew.sh/)** or **[MacPorts](https://www.macports.org/)** (you may have to add `/opt/local/bin/` to your PATH)
 * Run `brew install cairo pkg-config librsvg curl` or `port install cairo pkg-config librsvg curl`

## 2) Install opam

**opam** is a package manager for **Ocaml**.
**Dep2pict** requires **opam** version **2.0.0** or higher.

### On Ubuntu or Debian
The `apt` package manager does not currently (February 2019) provide **opam** version 2.

You should be able to install version **2.0.3** with the following commands:

  * `wget -q https://github.com/ocaml/opam/releases/download/2.0.3/opam-2.0.3-x86_64-linux`
  * `sudo mv opam-2.0.3-x86_64-linux /usr/local/bin/opam`
  * `sudo chmod a+x /usr/local/bin/opam`

For more information, please consult [**opam** installation page](https://opam.ocaml.org/doc/Install.html).

### On Mac OS&nbsp;X

**HomeBrew** and **MacPorts** propose **opam** version 2 by default.
You can install with

  * `brew install opam` or `port install opam`

## 3) Setup opam

Run `opam init` and follow instructions.
Note that it takes some times to download and build the `ocaml` compiler.

NB: some user have reporter that the command `opam init --disable-sandboxing` may avoid errors given by `opam init`.

Check that `ocaml` is installed with `ocamlc -v`.

## 4) Install Dep2pict software

```bash
opam remote add grew http://opam.grew.fr
opam install --yes dep2pict
```

# Installation of the PyQt GUI

A PyQt GUI is available (the `dep2pict` CLI must be installed separately).
The installation command for the GUI is:

```bash
pip install dep2pict-gui
```

Depending on your Python installation, you may have to replace `pip` by `pip3`.

You can check that it is well installed with the unix command `dep2pict-gui`.