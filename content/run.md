+++
Description = ""
date = "2017-02-28T14:58:11+01:00"
title = "run"
menu = "main"
Categories = ["Development","GoLang"]
Tags = ["Development","golang"]
+++

# Run Dep2pict

The tool is run with one of the three commands:

 * `dep2pict`: run the GUI
 * `dep2pict infile`: run the GUI and open `infile` in it
 * `dep2pict <options> infile outfile`: convert `infile` into `outfile`

The first two commands require that the unix command `dep2pict-gui` is available (see [Installation page](../installation)).

`infile` should be a CoNLL file. All aspects described in the [CoNLL-U format](http://universaldependencies.org/format.html) are supposed to be covered.

In the third command, `outfile` must have one of the extension `.svg`, `.pdf` or `.png` and available options are:

 * `-s <sent_id>` select the corresponding sentence to display
 * `-p <n>` when `n` is an integer: select the n<sup>th</sup> sentence in the input file

# Examples

The three examples below show how to produce the three graphs in the [main page](..).
## French example
An example with non-projective dependencies: sent_id `annodis.er_00204` in the trunk version of Sequoia corpus.

 * `wget https://gitlab.inria.fr/sequoia/deep-sequoia/raw/master/trunk/sequoia.surf.conll`
 * `dep2pict -s annodis.er_00204 sequoia.surf.conll fr.svg`

## English example
An example with empty node and enhanced dependencies: sent_id `reviews-071278-0003` in 2.0 version of UD_English.

 * `wget https://github.com/UniversalDependencies/UD_English/blob/r2.0/en-ud-train.conllu?raw=true -O en-ud-train.conllu`
 * `dep2pict -s reviews-071278-0003 en-ud-train.conllu en.svg`

## UD_Russian-SynTagRus
An example with empty node and enhanced dependencies: sent_id `637` in 2.0 version of UD_Russian-SynTagRus.

 * `wget https://github.com/UniversalDependencies/UD_Russian-SynTagRus/blob/r2.0/ru_syntagrus-ud-dev.conllu?raw=true -O ru_syntagrus-ud-dev.conllu`
 * `dep2pict -s 637 ru_syntagrus-ud-dev.conllu ru.svg`
